DROP DATABASE IF EXISTS pdf;

CREATE DATABASE pdf
DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
 
USE pdf;

-- tablas

DROP TABLE IF EXISTS imagen;
CREATE TABLE imagen(
    img_n INT(9) AUTO_INCREMENT PRIMARY KEY,
    link VARCHAR(200) NOT NULL /* dirección donde se ve la imagen y no la web */ 
	);


DROP TABLE IF EXISTS pregunta;
CREATE TABLE pregunta(
	pre_n INT (9) AUTO_INCREMENT PRIMARY KEY,
    cuestion TEXT NULL, /*Enunciado de la pregunta*/
	img_n INT(9)
	);
	

DROP TABLE IF EXISTS categoriap;
CREATE TABLE categoriap(
	cat_id int (9)  AUTO_INCREMENT PRIMARY KEY,
    cat_t varchar(40) NULL, /* nombre o tipo de la categoria de la pregunta individual*/ 
	pre_n INT (9) NOT NULL,
	img_n INT(9),
    UNIQUE KEY (cat_t, pre_n)
);


DROP TABLE IF EXISTS respuesta;
CREATE TABLE respuesta(
	res_n INT(9) AUTO_INCREMENT PRIMARY KEY, /* número de respuesta */
    letra CHAR(1) NOT NULL, /* posibilidad de una respuesta pudiendo ser a,b,c,d.. */
    pre_n INT(9) NOT NULL, /* número de la pregunta para esta respuesta */ 
    txtres TEXT NOT NULL,  /* redacción de la respuesta */
    correcta BOOLEAN NOT NULL, /* con 0 indica que es falsa y con 1 es verdadera */ 
	img_n INT(9),
    UNIQUE KEY (letra,pre_n)
	);
	
DROP TABLE IF EXISTS tipo_test;
CREATE TABLE tipo_test(
	tipo_n  INT(5) AUTO_INCREMENT PRIMARY KEY,
	nombre_tipo VARCHAR(55),
	img_n INT(9)
	);

DROP TABLE IF EXISTS fecha;
CREATE TABLE fecha(
	id_f  INT(9) AUTO_INCREMENT PRIMARY KEY,
	dia DATE,
	descripcion VARCHAR (80)
	);

DROP TABLE IF EXISTS nombretest;
CREATE TABLE nombretest(
	id_n  INT(9) AUTO_INCREMENT PRIMARY KEY,
	titulo VARCHAR (55),
	img_n INT(9)
	);



DROP TABLE IF EXISTS test;
CREATE TABLE test(
	id_t  INT(9) AUTO_INCREMENT PRIMARY KEY,
	id_n  INT(9), /* título del text de la tablas de nombrestest */
	pre_n INT(9), /*número de pregunta que lo compone*/
	tipo_n INT(5), /*corresponde al número del tipo de test*/
	id_f INT(9),
	UNIQUE KEY (id_n, pre_n, tipo_n,id_f)
	);



-- añadir más restricciones a las tablas

ALTER TABLE respuesta
ADD CONSTRAINT fk_respuesta_pregunta
FOREIGN KEY (`pre_n`) REFERENCES pregunta(`pre_n`),
ADD CONSTRAINT fk_respuesta_imagen
FOREIGN KEY (`img_n`) REFERENCES imagen(`img_n`);

ALTER TABLE pregunta
ADD CONSTRAINT fk_pregunta_imagen
FOREIGN KEY (`img_n`) REFERENCES imagen(`img_n`);

ALTER TABLE tipo_test
ADD CONSTRAINT fk_tipo_imagen
FOREIGN KEY (`img_n`) REFERENCES imagen(`img_n`);


ALTER TABLE categoriap
ADD CONSTRAINT fk_categoriap_pregunta
FOREIGN KEY (`pre_n`) REFERENCES pregunta(`pre_n`), 
ADD CONSTRAINT fk_categoriap_imagen
FOREIGN KEY (`img_n`) REFERENCES imagen(`img_n`);

ALTER TABLE nombretest
ADD CONSTRAINT fk_nombretest_imagen
FOREIGN KEY (`img_n`) REFERENCES imagen(`img_n`);


ALTER TABLE test
ADD CONSTRAINT fk_test_pregunta
FOREIGN KEY (`pre_n`)REFERENCES pregunta(`pre_n`),
ADD CONSTRAINT fk_test_tipo
FOREIGN KEY (`tipo_n`) REFERENCES `tipo_test`(`tipo_n`),
ADD CONSTRAINT fk_test_fecha
FOREIGN KEY (`id_f`) REFERENCES `fecha`(`id_f`),
ADD CONSTRAINT fk_test_nombretest
FOREIGN KEY (`id_n`) REFERENCES `nombretest`(`id_n`);
