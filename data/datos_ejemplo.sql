-- inserccion de datos
use pdf;

INSERT INTO `imagen` (`img_n`, `link`)
VALUES
(1,"https://img.icons8.com/ios/2x/jpg.png"),
(2,"https://img.icons8.com/plasticine/2x/ios-photos.png"),
(3,"https://img.icons8.com/wired/2x/ios-10.png"),
(4,"https://maxst.icons8.com/vue-static/icon/collection-favorites.png"),
(5,"https://img.icons8.com/officel/2x/ios-photos.png"),
(6,"https://img.icons8.com/color/2x/ios-themes.png"),
(7,"https://img.icons8.com/bubbles/2x/ios-photos.png"),
(8,"https://img.icons8.com/clouds/2x/ios-photos.png"),
(9,"https://img.icons8.com/wired/2x/ios-photos.png"),
(10,"https://img.icons8.com/ios/72/jake.png"),
(11,"https://img.icons8.com/ios-filled/72/jake.png"),
(12,"https://img.icons8.com/ios/2x/finn.png"),
(13,"https://img.icons8.com/ios/2x/ice-king.png"),
(14,"https://img.icons8.com/ios/2x/jake.png"),
(15,"https://img.icons8.com/ios/2x/lumpy-space-princess.png"),
(16,"https://img.icons8.com/ios/2x/marceline.png"),
(17,"https://img.icons8.com/ios/2x/princess-bubblegum.png"),
(18,"https://img.icons8.com/ios/2x/brutus.png"),
(19,"https://img.icons8.com/ios/2x/cheburashka.png"),
(20,"https://img.icons8.com/ios/2x/genie.png"),
(21,"https://img.icons8.com/ios/2x/kermit-the-frog.png"),
(22,"https://img.icons8.com/ios/2x/minion-1.png"),
(23,"https://img.icons8.com/ios/2x/mokona.png"),
(24,"https://img.icons8.com/ios/2x/moon-man.png"),
(25,"https://img.icons8.com/ios/2x/ninja-turtle.png"),
(26,"https://iconos8.es/icon/14929/popeye"),
(27,"https://iconos8.es/icon/63380/rick-sanchez"),
(28,"https://img.icons8.com/ios/2x/sailor-moon.png"),
(29,"https://img.icons8.com/ios/2x/woody-woodpecker.png"),
(30,"https://img.icons8.com/ios/2x/futurama-bender.png"),
(31,"https://img.icons8.com/ios/72/underline.png"),
(32,"https://icons8.com/icon/z_9J1JTZpLJg/incubator2"),
(33,"https://icons8.com/icon/74298/alpaca"),
(34,"https://icons8.com/icon/66428/aquarium"),
(35,"https://icons8.com/icon/49048/cat"),
(36,"https://icons8.com/icon/65960/dog-bowl"),
(37,"https://icons8.com/icon/48679/dog-house"),
(38,"https://icons8.com/icon/75tQDymwRs4-/dog-paw-print"),
(39,"https://icons8.com/icon/124233/guinea-pig"),
(40,"https://icons8.com/icon/106515/pets"),
(41,"https://icons8.com/icon/Y2SwL67gyiBR/poodle"),
(42,"https://icons8.com/icon/9pG5yLoBFelq/running-away"),
(43,"https://icons8.com/icon/49018/turtle"),
(44,"https://icons8.com/icon/PUiAeYhf-WzA/axolotl"),
(45,"https://icons8.com/icon/lzZGmKr2eQag/nautilus"),
(46,"https://icons8.com/icon/114750/orca"),
(47,"https://icons8.com/icon/73806/shark"),
(48,"https://icons8.com/icon/Vu4OQTOTCyMo/stingray"),
(49,"https://icons8.com/icon/62481/chimpanzee"),
(50,"https://icons8.com/icon/Npta5BprV_io/frog-face");


INSERT INTO `pregunta` (`pre_n`,`cuestion`)
VALUES
(1,"Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet,"),(2,"imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit,"),(3,"pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper,"),(4,"dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit"),(5,"ipsum porta elit, a feugiat tellus lorem eu metus. In"),(6,"luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget,"),(7,"hendrerit a, arcu. Sed et libero. Proin mi. Aliquam gravida"),(8,"Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper"),(9,"dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada"),(10,"lobortis tellus justo sit amet nulla. Donec non justo. Proin"),
(11,"lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,"),(12,"at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et"),(13,"ridiculus mus. Proin vel arcu eu odio tristique pharetra. Quisque"),(14,"vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at"),(15,"iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus"),(16,"eleifend egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum"),(17,"adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu"),(18,"senectus et netus et malesuada fames ac turpis egestas. Aliquam"),(19,"non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris"),(20,"ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit"),
(21,"pellentesque, tellus sem mollis dui, in sodales elit erat vitae"),(22,"diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris. Integer"),(23,"rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at,"),(24,"Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc"),(25,"orci, consectetuer euismod est arcu ac orci. Ut semper pretium"),(26,"porttitor tellus non magna. Nam ligula elit, pretium et, rutrum"),(27,"non leo. Vivamus nibh dolor, nonummy ac, feugiat non, lobortis"),(28,"In ornare sagittis felis. Donec tempor, est ac mattis semper,"),(29,"nunc sed libero. Proin sed turpis nec mauris blandit mattis."),(30,"amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus."),
(31,"tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer"),(32,"Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris,"),(33,"per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel"),(34,"quam a felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed"),(35,"nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut"),(36,"euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget"),(37,"posuere, enim nisl elementum purus, accumsan interdum libero dui nec"),(38,"vel arcu eu odio tristique pharetra. Quisque ac libero nec"),(39,"urna convallis erat, eget tincidunt dui augue eu tellus. Phasellus"),(40,"et malesuada fames ac turpis egestas. Fusce aliquet magna a"),
(41,"urna, nec luctus felis purus ac tellus. Suspendisse sed dolor."),(42,"at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et"),(43,"egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie"),(44,"ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat"),(45,"imperdiet nec, leo. Morbi neque tellus, imperdiet non, vestibulum nec,"),(46,"nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat,"),(47,"sem magna nec quam. Curabitur vel lectus. Cum sociis natoque"),(48,"velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor"),(49,"dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec,"),(50,"lorem, eget mollis lectus pede et risus. Quisque libero lacus,"),
(51,"ultrices sit amet, risus. Donec nibh enim, gravida sit amet,"),(52,"quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar"),(53,"tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a"),(54,"Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus"),(55,"euismod in, dolor. Fusce feugiat. Lorem ipsum dolor sit amet,"),(56,"luctus et ultrices posuere cubilia Curae; Phasellus ornare. Fusce mollis."),(57,"ac, fermentum vel, mauris. Integer sem elit, pharetra ut, pharetra"),(58,"lobortis, nisi nibh lacinia orci, consectetuer euismod est arcu ac"),(59,"Sed molestie. Sed id risus quis diam luctus lobortis. Class"),(60,"rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at,"),
(61,"dolor dapibus gravida. Aliquam tincidunt, nunc ac mattis ornare, lectus"),(62,"Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus,"),(63,"posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget,"),(64,"arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi."),(65,"at fringilla purus mauris a nunc. In at pede. Cras"),(66,"ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non,"),(67,"tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse"),(68,"eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus"),(69,"Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui,"),(70,"a, aliquet vel, vulputate eu, odio. Phasellus at augue id"),
(71,"malesuada fringilla est. Mauris eu turpis. Nulla aliquet. Proin velit."),(72,"nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis"),(73,"mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed"),(74,"in aliquet lobortis, nisi nibh lacinia orci, consectetuer euismod est"),(75,"facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa."),(76,"et ultrices posuere cubilia Curae; Phasellus ornare. Fusce mollis. Duis"),(77,"non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum."),(78,"sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor"),(79,"id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor,"),(80,"nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas."),
(81,"adipiscing, enim mi tempor lorem, eget mollis lectus pede et"),(82,"metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus"),(83,"mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent"),(84,"mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus"),(85,"per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel"),(86,"elementum, lorem ut aliquam iaculis, lacus pede sagittis augue, eu"),(87,"Donec porttitor tellus non magna. Nam ligula elit, pretium et,"),(88,"Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum"),(89,"lorem. Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis"),(90,"morbi tristique senectus et netus et malesuada fames ac turpis"),
(91,"est, mollis non, cursus non, egestas a, dui. Cras pellentesque."),(92,"amet lorem semper auctor. Mauris vel turpis. Aliquam adipiscing lobortis"),(93,"enim. Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare"),(94,"faucibus. Morbi vehicula. Pellentesque tincidunt tempus risus. Donec egestas. Duis"),(95,"Nam tempor diam dictum sapien. Aenean massa. Integer vitae nibh."),(96,"Nulla eget metus eu erat semper rutrum. Fusce dolor quam,"),(97,"Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem egestas"),(98,"sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices"),(99,"dictum placerat, augue. Sed molestie. Sed id risus quis diam"),(100,"Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula");


UPDATE `pregunta` SET`img_n`=1 WHERE `pre_n`=3;
UPDATE `pregunta` SET`img_n`=1 WHERE `pre_n`=9;
UPDATE `pregunta` SET`img_n`=2 WHERE `pre_n`=11;
UPDATE `pregunta` SET`img_n`=3 WHERE `pre_n`=16;
UPDATE `pregunta` SET`img_n`=3 WHERE `pre_n`=18;
UPDATE `pregunta` SET`img_n`=4 WHERE `pre_n`=20;
UPDATE `pregunta` SET`img_n`=5 WHERE `pre_n`=23;
UPDATE `pregunta` SET`img_n`=6 WHERE `pre_n`=25;
UPDATE `pregunta` SET`img_n`=7 WHERE `pre_n`=30;
UPDATE `pregunta` SET`img_n`=8 WHERE `pre_n`=32;
UPDATE `pregunta` SET`img_n`=8 WHERE `pre_n`=33;
UPDATE `pregunta` SET`img_n`=9 WHERE `pre_n`=37;
UPDATE `pregunta` SET`img_n`=10 WHERE `pre_n`=40;
UPDATE `pregunta` SET`img_n`=11 WHERE `pre_n`=43;
UPDATE `pregunta` SET`img_n`=12 WHERE `pre_n`=47;
UPDATE `pregunta` SET`img_n`=13 WHERE `pre_n`=49;
UPDATE `pregunta` SET`img_n`=13 WHERE `pre_n`=53;
UPDATE `pregunta` SET`img_n`=13 WHERE `pre_n`=54;
UPDATE `pregunta` SET`img_n`=14 WHERE `pre_n`=55;
UPDATE `pregunta` SET`img_n`=15 WHERE `pre_n`=59;
UPDATE `pregunta` SET`img_n`=16 WHERE `pre_n`=60;
UPDATE `pregunta` SET`img_n`=17 WHERE `pre_n`=65;
UPDATE `pregunta` SET`img_n`=18 WHERE `pre_n`=69;
UPDATE `pregunta` SET`img_n`=19 WHERE `pre_n`=70;
UPDATE `pregunta` SET`img_n`=20 WHERE `pre_n`=90;

INSERT INTO `categoriap` (`cat_t`,`pre_n`)
VALUES
("física","1"),("química","2"),("trigonometria","3"),("historia","4"),("álgebra","5"),
("lenguage","6"),("física","7"),("química","8"),("trigonometria","9"),("historia","10"),
("álgebra","11"),("lenguage","12"),("física","13"),("química","14"),("trigonometria","15"),
("historia","16"),("álgebra","17"),("lenguage","18"),("física","19"),("física","20"),
("word","21"),("word","22"),("word","23"),("word","24"),("word","25"),("word","26"),
("word","27"),("word","28"),("word","29"),("word","30"),("word","6"),("word","12"),("excel","31"),
("excel","32"),("excel","33"),("excel","34"),("excel","35"),("excel","36"),("excel","37"),
("excel","38"),("excel","39"),("excel","40"),("excel","15"),("power point","41"),("power point","42"),
("power point","43"),("power point","44"),("power point","45"),("power point","46"),
("power point","47"),("power point","48"),("power point","49"),("power point","50"),
("power point","51"),("power point","52"),("power point","53"),("power point","54"),
("geometria","55"),("geometria","56"),("geometria","57"),("geometria","58"),("geometria","59"),
("geometria","60"),("geometria","61"),("geometria","62"),("geometria","63"),("geometria","64"),
("geometria","65"),("geometria","66"),("geometria","67"),("geometria","68"),("geometria","69"),
("geometria","70"),("geometria","71"),("geometria","72"),("outlook","73"),("ubuntu","74"),
("geometria","75"),("geometria","76"),("geometria","77"),("geometria","78"),("historia","79"),
("historia","80"),("historia","81"),("historia","82"),("historia","83"),("historia","84"),
("historia","85"),("historia","86"),("sin categoria","87"),("sin categoria","88"),
("sin categoria","89"),("sin categoria","90"),("sin categoria","91"),("sin categoria","92"),
("sin categoria","93"),("sin categoria","94"),("sin categoria","95"),("sin categoria","96"),
("sin categoria","97"),("sin categoria","98"),("sin categoria","99"),("sin categoria","100");




INSERT INTO `respuesta` (`res_n`,`pre_n`,`letra`,`txtres`,`correcta`) VALUES 

(1,1,"a","vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum.",1),
(2,1,"b","Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae",0),
(3,1,"c","imperdiet nec, leo. Morbi neque tellus, imperdiet non, vestibulum nec,",0),

(4,2,"a","libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus",0),
(5,2,"b","Donec felis orci, adipiscing non, luctus sit amet, faucibus ut,",0),
(6,2,"c","mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin",1),

(7,3,"a","risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed",1),
(8,3,"b","Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam",1),
(9,3,"c","Ut tincidunt vehicula risus. Nulla eget metus eu erat semper",0),

(10,4,"a","leo. Morbi neque tellus, imperdiet non, vestibulum nec, euismod in,",0),
(11,4,"b","sit amet diam eu dolor egestas rhoncus. Proin nisl sem,",1),
(12,4,"c","ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit",0),

(13,5,"a","dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae,",1),
(14,5,"b","auctor odio a purus. Duis elementum, dui quis accumsan convallis,",0),
(15,5,"c","Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem,",0),

(16,6,"a","Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo",0),
(17,6,"b","turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed",0),
(18,6,"c","in consequat enim diam vel arcu. Curabitur ut odio vel",1),

(19,7,"a","senectus et netus et malesuada fames ac turpis egestas. Aliquam",0),
(20,7,"b","feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum",1),
(21,7,"c","ornare. Fusce mollis. Duis sit amet diam eu dolor egestas",0),

(22,8,"a","felis eget varius ultrices, mauris ipsum porta elit, a feugiat",1),
(23,8,"b","vel arcu eu odio tristique pharetra. Quisque ac libero nec",0),
(24,8,"c","risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi",0),

(25,9,"a","pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare",1),
(26,9,"b","adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis",0),
(27,9,"c","Ut tincidunt vehicula risus. Nulla eget metus eu erat semper",0),

(28,10,"a","leo elementum sem, vitae aliquam eros turpis non enim. Mauris",0),
(29,10,"b","rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in,",0),
(30,10,"c","Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum.",1),

(31,11,"a","velit dui, semper et, lacinia vitae, sodales at, velit. Pellentesque",0),
(32,11,"b","euismod ac, fermentum vel, mauris. Integer sem elit, pharetra ut,",1),
(33,11,"c","interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh",0),

(34,12,"a","sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla",0),
(35,12,"b","at pede. Cras vulputate velit eu sem. Pellentesque ut ipsum",0),
(36,12,"c","consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem,",1),

(37,13,"a","enim, condimentum eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin",0),
(38,13,"b","arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing.",1),
(39,13,"c","sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam",0),

(40,14,"a","luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed",0),
(41,14,"b","dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer",0),
(42,14,"c","eu odio tristique pharetra. Quisque ac libero nec ligula consectetuer",1),

(43,15,"a","tristique senectus et netus et malesuada fames ac turpis egestas.",1),
(44,15,"b","sem, vitae aliquam eros turpis non enim. Mauris quis turpis",0),
(45,15,"c","dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl.",0),

(46,16,"a","fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin",1),
(47,16,"b","Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec",0),
(48,16,"c","mattis ornare, lectus ante dictum mi, ac mattis velit justo",0),

(49,17,"a","Nam interdum enim non nisi. Aenean eget metus. In nec",0),
(50,17,"b","ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel",0),
(51,17,"c","Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum",1),

(52,18,"a","purus, in molestie tortor nibh sit amet orci. Ut sagittis",0),
(53,18,"b","in, dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer",1),
(54,18,"c","commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa",0),

(55,19,"a","tristique pellentesque, tellus sem mollis dui, in sodales elit erat",0),
(56,19,"b","ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor.",1),
(57,19,"c","suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante ipsum primis",0),

(58,20,"a","vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac",0),
(59,20,"b","aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in",1),
(60,20,"c","eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula.",0),

(61,21,"a","tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget",1),
(62,21,"b","neque non quam. Pellentesque habitant morbi tristique senectus et netus",0),
(63,21,"c","nisi nibh lacinia orci, consectetuer euismod est arcu ac orci.",0),

(64,22,"a","laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend,",1),
(65,22,"b","commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit,",0),
(66,22,"c","Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit,",0),

(67,23,"a","Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi",1),
(68,23,"b","in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu",0),
(69,23,"c","iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac,",0),

(70,24,"a","quis, pede. Praesent eu dui. Cum sociis natoque penatibus et",0),
(71,24,"b","odio a purus. Duis elementum, dui quis accumsan convallis, ante",0),
(72,24,"c","Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat",1),

(73,25,"a","tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer",0),
(74,25,"b","Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris",0),
(75,25,"c","eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum.",1),

(76,26,"a","nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante",0),
(77,26,"b","aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam",0),
(78,26,"c","Proin mi. Aliquam gravida mauris ut mi. Duis risus odio,",1),

(79,27,"a","dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu",0),
(80,27,"b","id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis",0),
(81,27,"c","non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec",1),

(82,28,"a","sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit",0),
(83,28,"b","ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam",1),
(84,28,"c","arcu vel quam dignissim pharetra. Nam ac nulla. In tincidunt",0),

(85,29,"a","aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque",0),
(86,29,"b","mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean",0),
(87,29,"c","Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare tortor",1),

(88,30,"a","Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros.",1),
(89,30,"b","gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit",0),
(90,30,"c","venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec",0),

(91,31,"a","Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus.",1),
(92,31,"b","dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum",0),
(93,31,"c","luctus et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae",0),

(94,32,"a","fringilla est. Mauris eu turpis. Nulla aliquet. Proin velit. Sed",0),
(95,32,"b","arcu. Curabitur ut odio vel est tempor bibendum. Donec felis",1),
(96,32,"c","Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue",0),

(97,33,"a","mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus",1),
(98,33,"b","a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque",0),
(99,33,"c","sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo.",0),

(100,34,"a","risus. In mi pede, nonummy ut, molestie in, tempus eu,",0),
(101,34,"b","ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel",0),
(102,34,"c","Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum",1),

(103,35,"b","vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum.",1),
(104,35,"a","Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae",0),
(105,35,"c","imperdiet nec, leo. Morbi neque tellus, imperdiet non, vestibulum nec,",0),

(106,36,"a","libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus",0),
(107,36,"b","Donec felis orci, adipiscing non, luctus sit amet, faucibus ut,",0),
(108,36,"c","mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin",1),

(109,37,"a","risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed",1),
(110,37,"b","Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam",1),
(111,37,"c","Ut tincidunt vehicula risus. Nulla eget metus eu erat semper",0),

(112,38,"a","leo. Morbi neque tellus, imperdiet non, vestibulum nec, euismod in,",0),
(113,38,"b","sit amet diam eu dolor egestas rhoncus. Proin nisl sem,",1),
(114,38,"c","ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit",0),

(115,39,"a","dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae,",1),
(116,39,"b","auctor odio a purus. Duis elementum, dui quis accumsan convallis,",0),
(117,39,"c","Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem,",0),

(118,40,"a","Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo",0),
(119,40,"b","turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed",0),
(120,40,"c","in consequat enim diam vel arcu. Curabitur ut odio vel",1),

(121,41,"a","senectus et netus et malesuada fames ac turpis egestas. Aliquam",0),
(122,41,"b","feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum",1),
(123,41,"c","ornare. Fusce mollis. Duis sit amet diam eu dolor egestas",0),

(124,42,"a","felis eget varius ultrices, mauris ipsum porta elit, a feugiat",1),
(125,42,"b","vel arcu eu odio tristique pharetra. Quisque ac libero nec",0),
(126,42,"c","risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi",0),

(127,43,"a","pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare",1),
(128,43,"b","adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis",0),
(129,43,"c","Ut tincidunt vehicula risus. Nulla eget metus eu erat semper",0),

(130,44,"a","leo elementum sem, vitae aliquam eros turpis non enim. Mauris",0),
(131,44,"b","rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in,",0),
(132,44,"c","Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum.",1),

(133,45,"a","velit dui, semper et, lacinia vitae, sodales at, velit. Pellentesque",0),
(134,45,"b","euismod ac, fermentum vel, mauris. Integer sem elit, pharetra ut,",1),
(135,45,"c","interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh",0),

(136,46,"a","sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla",0),
(137,46,"b","at pede. Cras vulputate velit eu sem. Pellentesque ut ipsum",0),
(138,46,"c","consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem,",1),

(139,47,"a","enim, condimentum eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin",0),
(140,47,"b","arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing.",1),
(141,47,"c","sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam",0),

(142,48,"a","luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed",0),
(143,48,"b","dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer",0),
(144,48,"c","eu odio tristique pharetra. Quisque ac libero nec ligula consectetuer",1),

(145,49,"a","tristique senectus et netus et malesuada fames ac turpis egestas.",1),
(146,49,"b","sem, vitae aliquam eros turpis non enim. Mauris quis turpis",0),
(147,49,"c","dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl.",0),

(148,50,"a","fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin",1),
(149,50,"b","Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec",0),
(150,50,"c","mattis ornare, lectus ante dictum mi, ac mattis velit justo",0),

(151,51,"a","Nam interdum enim non nisi. Aenean eget metus. In nec",0),
(152,51,"b","ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel",0),
(153,51,"c","Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum",1),

(154,52,"a","purus, in molestie tortor nibh sit amet orci. Ut sagittis",0),
(155,52,"b","in, dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer",1),
(156,52,"c","commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa",0),

(157,53,"a","tristique pellentesque, tellus sem mollis dui, in sodales elit erat",0),
(158,53,"b","ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor.",1),
(159,53,"c","suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante ipsum primis",0),

(160,54,"a","vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac",0),
(161,54,"b","aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in",1),
(162,54,"c","eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula.",0),

(163,55,"a","tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget",1),
(164,55,"b","neque non quam. Pellentesque habitant morbi tristique senectus et netus",0),
(165,55,"c","nisi nibh lacinia orci, consectetuer euismod est arcu ac orci.",0),

(166,56,"a","laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend,",1),
(167,56,"b","commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit,",0),
(168,56,"c","Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit,",0),

(169,57,"a","Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi",1),
(170,57,"b","in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu",0),
(171,57,"c","iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac,",0),

(172,58,"a","quis, pede. Praesent eu dui. Cum sociis natoque penatibus et",0),
(173,58,"b","odio a purus. Duis elementum, dui quis accumsan convallis, ante",0),
(174,58,"c","Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat",1),

(175,59,"a","tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer",0),
(176,59,"b","Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris",0),
(177,59,"c","eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum.",1),

(178,60,"a","nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante",0),
(179,60,"b","aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam",0),
(180,60,"c","Proin mi. Aliquam gravida mauris ut mi. Duis risus odio,",1),

(181,61,"a","dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu",0),
(182,61,"b","id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis",0),
(183,61,"c","non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec",1),

(184,62,"a","sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit",0),
(185,62,"b","ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam",1),
(186,62,"c","arcu vel quam dignissim pharetra. Nam ac nulla. In tincidunt",0),

(187,63,"a","aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque",0),
(188,63,"b","mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean",0),
(189,63,"c","Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare tortor",1),

(190,64,"a","Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros.",1),
(191,64,"b","gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit",0),
(192,64,"c","venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec",0),

(193,65,"a","Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus.",1),
(194,65,"b","dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum",0),
(195,65,"c","luctus et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae",0),

(196,66,"a","fringilla est. Mauris eu turpis. Nulla aliquet. Proin velit. Sed",0),
(197,66,"b","arcu. Curabitur ut odio vel est tempor bibendum. Donec felis",1),
(198,66,"c","Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue",0),

(199,67,"a","mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus",1),
(200,67,"b","a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque",0),
(201,67,"c","sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo.",0),

(202,68,"a","risus. In mi pede, nonummy ut, molestie in, tempus eu,",0),
(203,68,"b","ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel",0),
(204,68,"c","Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum",1),

(205,69,"b","vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum.",1),
(206,69,"a","Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae",0),
(207,69,"c","imperdiet nec, leo. Morbi neque tellus, imperdiet non, vestibulum nec,",0),

(208,70,"a","libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus",0),
(209,70,"b","Donec felis orci, adipiscing non, luctus sit amet, faucibus ut,",0),
(210,70,"c","mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin",1),

(211,71,"a","risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed",1),
(212,71,"b","Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam",1),
(213,71,"c","Ut tincidunt vehicula risus. Nulla eget metus eu erat semper",0),

(214,72,"a","leo. Morbi neque tellus, imperdiet non, vestibulum nec, euismod in,",0),
(215,72,"b","sit amet diam eu dolor egestas rhoncus. Proin nisl sem,",1),
(216,72,"c","ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit",0),

(217,73,"a","dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae,",1),
(218,73,"b","auctor odio a purus. Duis elementum, dui quis accumsan convallis,",0),
(219,73,"c","Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem,",0),

(220,74,"a","Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo",0),
(221,74,"b","turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed",0),
(222,74,"c","in consequat enim diam vel arcu. Curabitur ut odio vel",1),

(223,75,"a","senectus et netus et malesuada fames ac turpis egestas. Aliquam",0),
(224,75,"b","feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum",1),
(225,75,"c","ornare. Fusce mollis. Duis sit amet diam eu dolor egestas",0),

(226,76,"a","felis eget varius ultrices, mauris ipsum porta elit, a feugiat",1),
(227,76,"b","vel arcu eu odio tristique pharetra. Quisque ac libero nec",0),
(228,76,"c","risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi",0),

(229,77,"a","pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare",1),
(230,77,"b","adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis",0),
(231,77,"c","Ut tincidunt vehicula risus. Nulla eget metus eu erat semper",0),

(232,78,"a","leo elementum sem, vitae aliquam eros turpis non enim. Mauris",0),
(233,78,"b","rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in,",0),
(234,78,"c","Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum.",1),

(235,79,"a","velit dui, semper et, lacinia vitae, sodales at, velit. Pellentesque",0),
(236,79,"b","euismod ac, fermentum vel, mauris. Integer sem elit, pharetra ut,",1),
(237,79,"c","interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh",0),

(238,80,"a","sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla",0),
(239,80,"b","at pede. Cras vulputate velit eu sem. Pellentesque ut ipsum",0),
(240,80,"c","consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem,",1),

(241,81,"a","enim, condimentum eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin",0),
(242,81,"b","arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing.",1),
(243,81,"c","sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam",0),

(244,82,"a","luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed",0),
(245,82,"b","dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer",0),
(246,82,"c","eu odio tristique pharetra. Quisque ac libero nec ligula consectetuer",1),

(247,83,"a","tristique senectus et netus et malesuada fames ac turpis egestas.",1),
(248,83,"b","sem, vitae aliquam eros turpis non enim. Mauris quis turpis",0),
(249,83,"c","dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl.",0),

(250,84,"a","fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin",1),
(251,84,"b","Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec",0),
(252,84,"c","mattis ornare, lectus ante dictum mi, ac mattis velit justo",0),

(253,85,"a","Nam interdum enim non nisi. Aenean eget metus. In nec",0),
(254,85,"b","ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel",0),
(255,85,"c","Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum",1),

(256,86,"a","purus, in molestie tortor nibh sit amet orci. Ut sagittis",0),
(257,86,"b","in, dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer",1),
(258,86,"c","commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa",0),

(259,87,"a","tristique pellentesque, tellus sem mollis dui, in sodales elit erat",0),
(260,87,"b","ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor.",1),
(261,87,"c","suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante ipsum primis",0),

(262,88,"a","vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac",0),
(263,88,"b","aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in",1),
(264,88,"c","eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula.",0),

(265,89,"a","tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget",1),
(266,89,"b","neque non quam. Pellentesque habitant morbi tristique senectus et netus",0),
(267,89,"c","nisi nibh lacinia orci, consectetuer euismod est arcu ac orci.",0),

(268,90,"a","laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend,",1),
(269,90,"b","commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit,",0),
(270,90,"c","Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit,",0),

(271,91,"a","Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi",1),
(272,91,"b","in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu",0),
(273,91,"c","iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac,",0),

(274,92,"a","quis, pede. Praesent eu dui. Cum sociis natoque penatibus et",0),
(275,92,"b","odio a purus. Duis elementum, dui quis accumsan convallis, ante",0),
(276,92,"c","Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat",1),

(277,93,"a","tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer",0),
(278,93,"b","Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris",0),
(279,93,"c","eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum.",1),

(280,94,"a","nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante",0),
(281,94,"b","aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam",0),
(282,94,"c","Proin mi. Aliquam gravida mauris ut mi. Duis risus odio,",1),

(283,95,"a","dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu",0),
(284,95,"b","id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis",0),
(285,95,"c","non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec",1),

(286,96,"a","sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit",0),
(287,96,"b","ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam",1),
(288,96,"c","arcu vel quam dignissim pharetra. Nam ac nulla. In tincidunt",0),

(289,97,"a","aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque",0),
(290,97,"b","mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean",0),
(291,97,"c","Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare tortor",1),

(292,98,"a","Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros.",1),
(293,98,"b","gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit",0),
(294,98,"c","venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec",0),

(295,99,"a","Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus.",1),
(296,99,"b","dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum",0),
(297,99,"c","luctus et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae",0),

(298,100,"a","fringilla est. Mauris eu turpis. Nulla aliquet. Proin velit. Sed",0),
(299,100,"b","arcu. Curabitur ut odio vel est tempor bibendum. Donec felis",1),
(300,100,"c","Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue",0);

UPDATE `respuesta` SET `img_n`=21 WHERE `res_n`=11;
UPDATE `respuesta` SET `img_n`=22 WHERE `res_n`=18;
UPDATE `respuesta` SET `img_n`=23 WHERE `res_n`=25;
UPDATE `respuesta` SET `img_n`=24 WHERE `res_n`=32;
UPDATE `respuesta` SET `img_n`=25 WHERE `res_n`=43;
UPDATE `respuesta` SET `img_n`=26 WHERE `res_n`=87;
UPDATE `respuesta` SET `img_n`=27 WHERE `res_n`=112;
UPDATE `respuesta` SET `img_n`=28 WHERE `res_n`=193;
UPDATE `respuesta` SET `img_n`=29 WHERE `res_n`=271;
UPDATE `respuesta` SET `img_n`=30 WHERE `res_n`=291;

INSERT INTO `tipo_test` (`tipo_n`,`nombre_tipo`,`img_n`)
VALUES
(1,"1º EVALUACION",31),
(2,"2º EVALUACION",32),
(3,"3º EVALUACION",33),
(4,"FINAL",34),
(5,"RECUPERACION",35);

INSERT INTO `fecha` (`id_f`, `dia`, `descripcion`)
VALUES
(1,"2019-10-15","examen sorpresa"),
(2,"2019-10-25","museo"),
(3,"2019-11-02","test oral"),
(4,"2019-11-05","primera evaluación"),
(5,"2019-12-20","antes de navidad"),
(6,"2020-01-09","se oyen lo primero del coronavirus"),
(7,"2020-01-19","la gripe viene igual"),
(8,"2020-01-19","examen segunda evaluacion"),
(9,"2020-02-05","la gripe parece seria"),
(10,"2020-02-15","los chinos arrasan con las mascarillas"),
(11,"2020-02-27","ui ui"),
(12,"2020-03-05","los noticiarios r que r"),
(13,"2020-03-10","se habla que en italia estan joios"),
(14,"2020-03-12","se anula todo en españa"),
(15,"2020-03-15","estado de alarma"),
(16,"2020-03-19","todos en casa"),
(17,"2020-03-28","la cosa esta joia"),
(18,"2020-04-05","que stress dios mio"),
(19,"2020-04-19","alarma y alarma"),
(20,"2020-05-20","se puede salir algo");

INSERT INTO `nombretest` (`id_n`,`titulo`,`img_n`)
VALUES
(1,"matematicas",36),
(2,"lenguage",37),
(3,"filosofia",38),
(4,"naturales",39),
(5,"tecnologia",40),
(6,"religión",41),
(7,"gimnasia",42),
(8,"medio ambiente",43),
(9,"prácticas",NULL),
(10,"oratoria",44),
(11,"programación",45),
(12,"historia",NULL),
(13,"laboratorio",NULL),
(14,"diseño gráfico",NULL),
(15,"arquitectura de ordenadores",NULL),
(16,"seguridad e higiene",NULL),
(17,"sociales",NULL);

INSERT INTO `test` (`id_t`,`tipo_n`,`id_n`,`id_f`,`pre_n`)
VALUES
(1,1,1,1,1),
(2,1,1,1,2),
(3,1,1,1,3),
(4,1,1,1,4),
(5,1,1,1,5),
(6,1,1,1,6),
(7,1,1,1,7),
(8,1,1,1,8),
(9,1,1,1,9),
(10,3,5,2,10),
(11,3,5,2,21),
(12,3,5,2,22),
(13,3,5,2,23),
(14,3,5,2,24),
(15,3,5,2,25),
(16,3,5,2,26),
(17,3,5,2,27),
(18,3,5,2,28),
(19,3,5,2,29),
(20,3,5,2,30),
(21,5,2,4,41),
(22,5,2,4,42),
(23,5,2,4,43),
(24,5,2,4,44),
(25,5,2,4,45),
(26,5,2,4,46),
(27,5,2,4,47),
(28,5,2,4,48),
(29,5,2,4,49),
(30,5,2,4,50);

